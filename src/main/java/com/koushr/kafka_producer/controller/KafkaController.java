package com.koushr.kafka_producer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.koushr.kafka_producer.init.KafkaConnector;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping(value = "/kafka")
public class KafkaController {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

    private static final Random random = new Random();

    @Autowired
    private KafkaConnector kafkaConnector;

    @GetMapping(value = "/produce1")
    public Map<String, Object> produce1() throws IOException {
        String str = "{\"ip_port\":\"10.10.40.90:7002\",\"endpoint_id\":\"008eb8108b81ad62ef8d224f4191d3d8\",\"endpoint\":\"{POST}/{0}/{1}/login.do\",\"vulnerability_id\":\"64d1b697fef6ac47fa215545\",\"vulnerability_name\":\"明文密码接口\",\"vulnerability_desc\":\"接口传输密码使用明文，攻击者若在传输过程中进行劫持或监听，就可以轻松截获到用户密码\",\"vulnerability_updated_at\":\"2023-08-08T03:29:27.233Z\",\"fix_advice\":\"建议密码在传输时进行加密传输，可使用哈希(HASH)或哈希加盐法(HASH+SALT)进行处理\",\"risk_level\":\"HIGH\",\"handle_status\":\"PENDING\",\"vulnerability_features\":[\"请求体：password=pleasemygod2008\"],\"request_id\":\"NL:d27d96d5-5f90-4327-b7e8-ea556c219b01_20230808114555\",\"url\":{\"hit\":false,\"data\":\"http://161.156.45.160:7002/dxodpxaxkr/weakpassword94769/login.do\"},\"req_header\":{\"hit\":false,\"data\":\"{\\\\\\\"peer\\\\\\\":[\\\\\\\"161.156.45.160\\\\\\\"],\\\\\\\"host\\\\\\\":[\\\\\\\"161.156.45.160:7002\\\\\\\"],\\\\\\\"content-type\\\\\\\":[\\\\\\\"application/json; charset=UTF-8 \\\\\\\"],\\\\\\\"connection\\\\\\\":[\\\\\\\"keep-alive\\\\\\\"],\\\\\\\"user-agent\\\\\\\":[\\\\\\\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.612.0 Safari/537.36 \\\\\\\"],\\\\\\\"authorization\\\\\\\":[\\\\\\\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6ImdlbmVyYXRldXNlcmQiLCJpYXQiOjEyMzQ1Njc4OTB9.y88TJXKPH4kFEYlUyxzMZd1SKQX20eB7aANna5V9X6E\\\\\\\"],\\\\\\\"content-length\\\\\\\":[\\\\\\\"504\\\\\\\"]}\"},\"req_body\":{\"hit\":true,\"data\":\"{\\r\\n \\\\\\\"username\\\\\\\":\\\\\\\"admin\\\\\\\",\\r\\n \\\\\\\"password\\\\\\\":\\\\\\\"pleasemygod2008\\\\\\\",\\r\\n \\\\\\\"name\\\\\\\": \\\\\\\"test6411\\\\\\\",\\r\\n \\\\\\\"url\\\\\\\": \\\\\\\"http://www.baidu.com\\\\\\\",\\r\\n \\\\\\\"age\\\\\\\": \\\\\\\"20\\\\\\\",\\r\\n \\\\\\\"first\\\\\\\": \\\\\\\"习近平\\\\\\\",\\r\\n \\\\\\\"mobile\\\\\\\": \\\\\\\"13456894743\\\\\\\",\\r\\n \\\\\\\"address\\\\\\\": \\\\\\\"广东省深圳市软件产业基地\\\\\\\",\\r\\n \\\\\\\"city\\\\\\\": \\\\\\\"深圳市\\\\\\\",\\r\\n \\\\\\\"create_time\\\\\\\": \\\\\\\"2023-04-23 11:00:00\\\\\\\",\\r\\n \\\\\\\"first\\\\\\\": \\\\\\\"习近平\\\\\\\",\\r\\n \\\\\\\"id\\\\\\\": \\\\\\\"1\\\\\\\",\\r\\n \\\\\\\"mobile\\\\\\\": \\\\\\\"13359762059\\\\\\\",\\r\\n \\\\\\\"position\\\\\\\": \\\\\\\"京F00032\\\\\\\",\\r\\n \\\\\\\"province\\\\\\\": \\\\\\\"广东省\\\\\\\",\\r\\n \\\\\\\"remark\\\\\\\": \\\\\\\"115.79.140.210\\\\\\\",\\r\\n \\\\\\\"second\\\\\\\": \\\\\\\"胡锦涛\\\\\\\",\\r\\n \\\\\\\"sex\\\\\\\": \\\\\\\"男\\\\\\\",\\r\\n \\\\\\\"third\\\\\\\": \\\\\\\"深圳\\\\\\\"\\r\\n}\"},\"res_header\":{\"hit\":false,\"data\":\"{\\\\\\\"date\\\\\\\":[\\\\\\\"Tue, 08 Aug 2023 03:45:54 GMT\\\\\\\"],\\\\\\\"keep-alive\\\\\\\":[\\\\\\\"timeout=60\\\\\\\"],\\\\\\\"transfer-encoding\\\\\\\":[\\\\\\\"chunked\\\\\\\"],\\\\\\\"connection\\\\\\\":[\\\\\\\"keep-alive\\\\\\\"],\\\\\\\"content-type\\\\\\\":[\\\\\\\"application/json;charset=UTF-8\\\\\\\"]}\"},\"res_body\":{\"hit\":false,\"data\":\"{ \\\\\\\"username\\\\\\\":\\\\\\\"admin\\\\\\\", \\\\\\\"password\\\\\\\":\\\\\\\"pleasemygod2008\\\\\\\", \\\\\\\"name\\\\\\\": \\\\\\\"test6411\\\\\\\", \\\\\\\"url\\\\\\\": \\\\\\\"http://www.baidu.com\\\\\\\", \\\\\\\"age\\\\\\\": \\\\\\\"20\\\\\\\", \\\\\\\"first\\\\\\\": \\\\\\\"习近平\\\\\\\", \\\\\\\"mobile\\\\\\\": \\\\\\\"13456894743\\\\\\\", \\\\\\\"address\\\\\\\": \\\\\\\"广东省深圳市软件产业基地\\\\\\\", \\\\\\\"city\\\\\\\": \\\\\\\"深圳市\\\\\\\", \\\\\\\"create_time\\\\\\\": \\\\\\\"2023-04-23 11:00:00\\\\\\\", \\\\\\\"first\\\\\\\": \\\\\\\"习近平\\\\\\\", \\\\\\\"id\\\\\\\": \\\\\\\"1\\\\\\\", \\\\\\\"mobile\\\\\\\": \\\\\\\"13359762059\\\\\\\", \\\\\\\"position\\\\\\\": \\\\\\\"京F00032\\\\\\\", \\\\\\\"province\\\\\\\": \\\\\\\"广东省\\\\\\\", \\\\\\\"remark\\\\\\\": \\\\\\\"115.79.140.210\\\\\\\", \\\\\\\"second\\\\\\\": \\\\\\\"胡锦涛\\\\\\\", \\\\\\\"sex\\\\\\\": \\\\\\\"男\\\\\\\", \\\\\\\"third\\\\\\\": \\\\\\\"深圳\\\\\\\"}\"},\"req_sens_data_tags\":{},\"resp_sens_data_tags\":{},\"created_at\":\"2023-08-08T15:37:03.039217293+08:00\",\"created_at_timestamp\":1691480223039,\"updated_at\":\"2023-08-08T15:37:03.039217563+08:00\"}";
        log.info("kafka produce start");
        for (int j = 0; j < Integer.MAX_VALUE; j++) {
            Map map = objectMapper.readValue(str, Map.class);
            map.put("endpoint_id", UUID.randomUUID().toString());
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>("vulnerabilities", objectMapper.writeValueAsString(map));
            kafkaConnector.getProducer().send(producerRecord);
//                kafkaConnector.getProducer().send(producerRecord, (RecordMetadata metadata, Exception exception) -> {
//                    if (exception != null) {
//                        log.info("exception= " + exception);
//                    }
//                });
//                try {
//                    Thread.sleep(20);
//                } catch (InterruptedException e) {
//                    throw new RuntimeException(e);
//                }
        }
        log.info("kafka produce end");
        return ImmutableMap.of("status", "success");
    }


    @GetMapping(value = "/produce2")
    public Map<String, Object> produce2() throws IOException {
        String str = "{\"businessCode\":\"admin\",\"emailStatus\":2,\"eventRemark\":\"系统账号admin连续登陆时间周期1.0分钟，账号登录失败次数2次，疑似账号爆破，请及时查看和处理\",\"mailTitle\":\"疑似账号爆破\",\"messageStatus\":2,\"notifyTactics\":\"消息通知,邮件通知\",\"reportEventParamMap\":{\"loginFailNumber\":\"2\",\"sysUserName\":\"admin\",\"minutes\":\"1\"},\"reportEventTime\":\"2023-08-08T21:05:58\",\"reportName\":\"疑似账号爆破\",\"reportObject\":\"admin\",\"reportType\":\"运维监控\"}";
        log.info("kafka produce start");
        for (int j = 0; j < Integer.MAX_VALUE; j++) {
            Map map = objectMapper.readValue(str, Map.class);
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>("report-event-message", objectMapper.writeValueAsString(map));
            kafkaConnector.getProducer().send(producerRecord);
//                kafkaConnector.getProducer().send(producerRecord, (RecordMetadata metadata, Exception exception) -> {
//                    if (exception != null) {
//                        log.info("exception= " + exception);
//                    }
//                });
//                try {
//                    Thread.sleep(20);
//                } catch (InterruptedException e) {
//                    throw new RuntimeException(e);
//                }
        }
        log.info("kafka produce end");
        return ImmutableMap.of("status", "success");
    }

    @GetMapping(value = "/produce3")
    public Map<String, Object> produce3() throws IOException {
        String str = "{\"createBy\":\"cd51b7f4-17aa-42b5-a03e-099e48afb0f4_test\",\"createTime\":\"2023-08-09T14:17:32.141857024\",\"description\":\"全局配置-监控设置-日志转发配置-修改\",\"id\":\"64d32f7c97e4d1296d99b2c5\",\"ip\":\"10.10.12.250\",\"logType\":\"业务操作\",\"remark\":\"Pipe has been updated successfully!!!\",\"result\":0,\"updateTime\":\"2023-08-09T14:17:32.141857024\",\"userName\":\"admin\"}";
        log.info("kafka produce start");
        for (int j = 0; j < Integer.MAX_VALUE; j++) {
            Map map = objectMapper.readValue(str, Map.class);
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>("sys-op-log", objectMapper.writeValueAsString(map));
            kafkaConnector.getProducer().send(producerRecord);
//                kafkaConnector.getProducer().send(producerRecord, (RecordMetadata metadata, Exception exception) -> {
//                    if (exception != null) {
//                        log.info("exception= " + exception);
//                    }
//                });
//                try {
//                    Thread.sleep(20);
//                } catch (InterruptedException e) {
//                    throw new RuntimeException(e);
//                }
        }
        log.info("kafka produce end");
        return ImmutableMap.of("status", "success");
    }
}